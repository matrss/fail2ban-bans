import attr
import requests as req

from datetime import datetime


@attr.s
class GeoData:
    retrieved_at = attr.ib()
    hostname = attr.ib()
    ip = attr.ib()
    country = attr.ib()
    region = attr.ib()
    city = attr.ib()
    postal = attr.ib()
    latitude = attr.ib()
    longitude = attr.ib()
    timezone = attr.ib()
    org = attr.ib()

    def __iter__(self):
        return iter(
            [
                self.retrieved_at.isoformat(),
                self.ip,
                self.hostname,
                self.country,
                self.region,
                self.city,
                self.postal,
                self.latitude,
                self.longitude,
                self.timezone,
                self.org,
            ]
        )

    @classmethod
    def from_ip(cls, ip):
        response = req.get(
            "https://ipinfo.io/{}/json".format(ip),
        )
        if not response.ok:
            raise RuntimeError()
        data = response.json()
        data["retrieved_at"] = datetime.now()
        loc = data.pop("loc")
        lat, lon = loc.split(",")
        data["latitude"] = lat
        data["longitude"] = lon
        return cls(**data)


@attr.s
class BanEntry:
    banned_at = attr.ib()
    ip = attr.ib()
    geo = attr.ib(init=False)

    @geo.default
    def _ip_geo_lookup(self):
        return GeoData.from_ip(self.ip)

    @classmethod
    def parse_from_f2b(cls, msg):
        msg = [str(datetime.now().year)] + msg.split(" ")
        banned_at = datetime.strptime(" ".join(msg[:4]), "%Y %b %d %H:%M:%S")
        ip = msg[-1]
        return cls(banned_at, ip)

    @classmethod
    def csv_header(cls):
        return "ip,banned_at,retrieved_at,geo_ip,hostname,country,region,city,postal,latitude,longitude,timezone,org"

    def __iter__(self):
        return iter([self.ip, self.banned_at.isoformat()] + list(iter(self.geo)))
