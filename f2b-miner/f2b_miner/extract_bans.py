import sys
import csv
import argparse
import subprocess
import trace

from pathlib import Path
from .datatypes import BanEntry


def is_ban(msg):
    return "NOTICE [sshd] Ban" in msg


def run(input_stream, output_stream):
    for msg in input_stream:
        msg = msg.rstrip("\n").rstrip("\r")

        if not is_ban(msg):
            continue

        ban_entry = BanEntry.parse_from_f2b(msg)

        c = csv.writer(output_stream)
        c.writerow(ban_entry)
        output_stream.flush()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("output_file", help="Path to the output file, use - for stdout")
    args = parser.parse_args()

    with subprocess.Popen(
        [
            "journalctl",
            "-u",
            "fail2ban.service",
            "--since",
            "now",
            "-f",
        ],
        text=True,
        stdout=subprocess.PIPE,
    ) as journal_f2b:
        if args.output_file == "-":
            print(BanEntry.csv_header())
            run(journal_f2b.stdout, sys.stdout)
        else:
            output_file = Path(args.output_file)
            write_header = not output_file.exists()
            with open(args.output_file, mode="a") as f:
                if write_header:
                    f.write(BanEntry.csv_header())
                    f.write("\n")
                f.flush()
                run(journal_f2b.stdout, f)
