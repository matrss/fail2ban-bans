{
  description = "Tools to extract bans from fail2ban and analyze them.";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    poetry2nix.url = "github:nix-community/poetry2nix";
  };

  outputs = { self, nixpkgs, flake-utils, poetry2nix }: flake-utils.lib.eachDefaultSystem
    (system:
      let
        pkgs = import nixpkgs { inherit system; overlays = [ poetry2nix.overlay ]; };
      in
      {
        packages = self.overlay pkgs pkgs;

        devShell = with pkgs; with self.packages."${system}"; mkShell {
          inputsFrom = [ f2b-miner ];
          buildInputs = [ poetry f2b-miner ];
        };
      }) // {

    nixosModules = {
      f2b-miner = import ./nix/modules/f2b-miner.nix;
    };

    overlay = nixpkgs.lib.composeManyExtensions [
      poetry2nix.overlay
      (final: prev: {
        f2b-miner = prev.poetry2nix.mkPoetryApplication { projectDir = ./f2b-miner; };
      })
    ];
  };
}
