{ lib, pkgs, config, ... }:

with lib;

let
  cfg = config.services.f2b-miner;
in
{
  options.services.f2b-miner = {
    enable = mkEnableOption "f2b-miner";

    logFile = mkOption {
      type = types.str;
      description = "Log file to write output to.";
    };

    # f2bLogSource = mkOption {
    #   type = types.str;
    #   default = "${pkgs.systemd}/bin/journalctl -u fail2ban.service --since '2021-04-27 20:00:00' -f";
    #   description = "Source for the fail2ban log, should be a shell command that prints the log to stdout.";
    # };

    user = mkOption {
      type = types.str;
      description = "User account under which f2b-miner runs.";
    };

    group = mkOption {
      type = types.str;
      description = "Group under which f2b-miner runs.";
    };

  };

  config = mkIf cfg.enable {
    systemd.services.f2b-miner = {
      wantedBy = [ "multi-user.target" ];
      serviceConfig = {
        Type = "simple";
        Restart = "always";
        User = cfg.user;
        Group = cfg.group;
        # ExecStart = "${pkgs.dash}/bin/dash -c \"${cfg.f2bLogSource} | ${pkgs.f2b-miner}/bin/f2b-miner ${cfg.logFile}\"";
        ExecStart = "${pkgs.f2b-miner}/bin/f2b-miner ${cfg.logFile}";
      };
    };
  };
}
