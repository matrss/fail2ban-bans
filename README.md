## What is this?
This project is an effort to collect data about ip addresses
that got banned by `fail2ban` on my server.
It aims to provide the tools to generate easily processable data from `fail2ban`s log
and run some analysis on that.

It is still a work-in-progress.

## And why is this?
As someone who is running a publicly accessible server with ssh on the default port
(obviously public key authentication only) I noticed
that I get **a lot** of authentication failures from random IPs,
presumably from all over the world.
A quick look revealed more than 1300 bans in a timeframe of about 1 month,
which roughly translates to more than 40 per day.

I want to find out where those attacks are coming from,
if there might be some origin clusters,
if there are patterns in for example the connection times
and whatever else I might think of.

## Data Sources
- fail2ban log on my server
- [ipinfo.io](https://ipinfo.io)
